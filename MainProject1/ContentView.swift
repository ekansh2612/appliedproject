//
//  ContentView.swift
//  MainProject1
//
//  Created by Ekansh Sharma on 2019-09-24.
//  Copyright © 2019 Ekansh Sharma. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello World")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
